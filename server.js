'use strict';


//app.set('port', )
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

//app.listen(5000);
var mongoose = require('./config/mongoose')(),
    app = require('./config/express')();


// 몽고 디비 연결
//var db = mongoose();


var portNum = 5000;
var server = app.listen(5000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Korean Tutor App listening at http://%s:%s', host, port);
});

