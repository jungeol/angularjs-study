/**
 * Created by es-mac on 2015. 11. 27..
 */
'use strict';

var passport = require('passport');


var User = require('../app/models/user.server.model');

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findOne({
        _id: id
    }, '-password -salt', function (err, user) {
        done(err, user);
    });
});

require('./strategies/local')(passport);


module.exports = passport;
