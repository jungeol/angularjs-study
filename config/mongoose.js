'use strict';

var	config = require('./config'),
    mongoose = require('mongoose');

module.exports = function() {
    // ��� ��� ����

    var db = mongoose.connect(config.db);


    var con = mongoose.connection;

    con.on('error', console.error.bind(console, 'connection error:'));
    // 몽구즈 에러처리 예제
    //var mongoose = require('mongoose');
    //
    //var db = mongoose.connection;
    //
    //db.on('error', console.error);
    //db.once('open', function() {
    //    // Create your schemas and models here.
    //});

    //mongoose.connect('mongodb://localhost/test');
    // Load the application models
    require('../app/models/user.server.model');
    require('../app/models/tutor.server.model');
    require('../app/models/student.server.model');


    return db;
};
