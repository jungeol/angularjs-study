/**
 * Created by es-mac on 15. 11. 14..
 */
// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies

var LocalStrategy = require('passport-local').Strategy,
    User = require('../../app/models/user.server.model'),
    Tutor = require('../../app/models/tutor.server.model'),
    Student = require('../../app/models/student.server.model');

// Create the Local strategy configuration method
module.exports = function (passport) {


    var userRequiredFieldCheck = function (req, done) {

    };

    passport.use('local-signup', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {

            process.nextTick(function () {
                User.findOne({'local.email': email}, function (err, user) {
                    if (err)
                        return done(err);

                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {

                        var newUser = new User();
                        newUser.local.email = email;
                        newUser.local.password = newUser.generateHash(password);

                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });

                    }

                });
            });
        }
    ));

    passport.use('local-login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {
            User.findOne({'email': email}, function (err, user) {

                if (err) {
                    console.log(err);
                    return done(err);
                }


                console.dir(req.body);
                if (!user) {
                    console.error('No user found');
                    return done(null, false, req.flash('loginMessage', 'No user found'));
                }


                if (!user.hashPassword(password)) {
                    console.error('Wrong Password');
                    return done(null, false, req.flash('loginMessage', 'Wrong passsword.'));
                }


                return done(null, user);
            });
        }));

    passport.use('local-signup-tutor', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {

            process.nextTick(function () {
                User.findOne({'email': email}, function (err, user) {
                    if (err) {
                        console.log(err);
                        return done(err);
                    }
                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {

                        var newUser = new User(req.body);
                        console.log("튜터 가입 요청");
                        console.dir(req.body);

                        newUser.save(function (err) {
                            if (err) {
                                console.log(err);
                                throw err;
                            }
                        });

                        var newTutor = new Tutor();
                        newTutor.user_id = newUser._id;
                        switch (req.body.addr) {
                            case 'yuseongu':
                                newTutor.addr.yuseongu = true;
                                break;
                            case 'seogu':
                                newTutor.addr.dongu = true;
                                break;
                            case 'daedukgu':
                                newTutor.addr.daedukgu = true;
                                break;
                            case 'dongu':
                                newTutor.addr.dongu = true;
                                break;
                            case 'jungu':
                                newTutor.addr.jungu = true;
                                break;
                        }

                        newTutor.cert_level = req.body.certlevel;
                        newTutor.save(function (err, tutor) {
                            if (err) {
                                newUser.remove(function (err, user) {
                                    if (err)
                                        return handleError(err);
                                    User.findById(user._id, function (err, user) {
                                        console.log(user);
                                    })
                                });
                                return console.dir(err);
                            }
                            console.info("튜터 생성");
                            console.log(tutor);
                        })


                    }

                });
            });
        }
    ));

    passport.use('local-signup-student', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {
            console.log("리퀘스트 출력");
            console.dir(req.body);
            process.nextTick(function () {
                User.findOne({'email': email}, function (err, user) {
                    if (err) {
                        console.log("DB 에러");
                        console.log(err);
                        return done(err);
                    }
                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {

                        var newUser = new User(req.body);
                        console.log("학생 가입 요청");
                        console.dir(req.body);

                        newUser.save(function (err) {
                            if (err) {
                                console.log(err);
                                throw err;
                            }
                        });

                        var newStudent = new Student();
                        newStudent.user_id = newUser._id;
                        switch (req.body.addr) {
                            case 'yuseongu':
                                newStudent.addr.yuseongu = true;
                                break;
                            case 'seogu':
                                newStudent.addr.dongu = true;
                                break;
                            case 'daedukgu':
                                newStudent.addr.daedukgu = true;
                                break;
                            case 'dongu':
                                newStudent.addr.dongu = true;
                                break;
                            case 'jungu':
                                newStudent.addr.jungu = true;
                                break;
                        }


                        newStudent.save(function (err, student) {
                            if (err) {
                                newUser.remove(function (err, user) {
                                    if (err)
                                        return handleError(err);
                                    User.findById(user._id, function (err, user) {
                                        console.log(user);
                                    })
                                });
                                return console.dir(err);
                            }
                            console.info("학생 생성");
                            console.log(student);
                        })


                    }

                });
            });
        }
    ));
};

