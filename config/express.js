var config = require('./config'),
    express = require('express'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    flash = require('connect-flash'),
    morgan = require('morgan'),
    path = require('path'),
    session = require('express-session'),
    index = require('../app/routes/index.server.routes'),
    students = require('../app/routes/student.server.routes'),
    tutors = require('../app/routes/tutor.sever.routes'),
    passport = require('../config/passport'),
    api = require('../app/routes/api.server.routes');

// 파일 업로드 부분
var multer = require('multer'),
    upload = multer({dest: __dirname + '../media/'});


module.exports = function () {
    var app = express();

    if (process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
    } else if (process.env.NODE_ENV === 'production') {
        app.use(compress());
    }

    app.use(cookieParser());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));


    // View 설정
    app.set('views', path.join(__dirname, '../app/views'));
    app.set('view engine', 'ejs');

    app.use(session({secret: 'iloveprogramming'}));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());

    // Static folder 설정
    app.use('/public', express.static(path.join(__dirname, '../public')));
    app.use('/media', express.static(path.join(__dirname, "../media")));


    // 라우트 설정
    app.use('/', index);
    //app.use('/students', students);
    //app.use('/tutors', tutors);

    // API설정
    app.use('/api', api);


    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // Development 환경시 에러처리 로직
    if (app.get('env') === 'development') {
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            console.log(err);
            res.render('error', {
                message: err.message,
                error: err
            });
        });
    }

    // 기타 서버 환경 에러 처리
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });


    return app;
};