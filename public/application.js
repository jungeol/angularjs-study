// Invoke 'strict' JavaScript mode
'use strict';

// 메인 에플리케이션 이름
var mainApplicationModuleName = 'KrTutor';

// 메인 어플리케이션 생성
//var mainApplicationModule = angular.module(mainApplicationModuleName,
//    ['ngResource', 'ngRoute', 'ui.bootstrap', 'students', 'tutors']);

var mainApplicationModule = angular.module(mainApplicationModuleName,
    ['ngResource', 'ngRoute']);

// Configure the hashbang URLs using the $locationProvider services
mainApplicationModule.config(['$locationProvider',
    function ($locationProvider) {
        $locationProvider.hashPrefix('!');
    }
]);

// Configure the universal filters
//mainApplicationModule
//    .filter("range", function ($filter) {
//        return function (data, page, size) {
//            if (angular.isArray(data) && angular.isNumber(page) && angular.isNumber(size)) {
//                var start_index = (page - 1) * size;
//                if (data.length < start_index) {
//                    return [];
//                } else {
//                    return $filter("limitTo")(data.splice(start_index), size);
//                }
//            } else {
//                return data;
//            }
//        }
//    })
//    .filter("pageCount", function () {
//        return function (data, size) {
//            if (angular.isArray(data)) {
//                var result = [];
//                for (var i = 0; i < Math.ceil(data.length / size); i++) {
//                    result.push(i);
//                }
//                return result;
//            } else {
//                return data;
//            }
//        }
//    });


// Fix Facebook's OAuth bug
if (window.location.hash === '#_=_') window.location.hash = '#!';

// Manually bootstrap the AngularJS application
angular.element(document).ready(function () {
    angular.bootstrap(document, [mainApplicationModuleName]);
});