angular.module("bookmark",[])
.factory("bookmark",function(){
  var bookmarkData = [];

  return {
    addBookmark: function(data){
        var existingData = false;
        for(var i=0; i<bookmarkData.length; i++){
          if(bookmarkData[i].id == data.id){
            existingData = true;
            break;
          }
        }
        if(!existingData){
          data.date = new Date();
          bookmarkData.push(data);
        }
    },

    removeBookmark: function(data){
      for(var i=0; i<bookmarkData.length; i++){
        if(bookmarkData[i].id == data.id){
          bookmarkData.splice(i,1);
          break;
        }
      }
    },

    getBookmarks: function(){
      return bookmarkData;
    }
  }
})
.directive("bookmarkSummary", function(bookmark){
  return {
    restrict:"E",
    templateUrl:"components/bookmark/bookmarkSummary.html",
    controller:function($scope){
      var bookmarkData = bookmark.getBookmarks();

      $scope.bookmarkCount = function(){
        var total = 0;
        for(var i=0; i<bookmarkData.length; i++){
          total += 1;
        }
        return total;
      }
    }
  };
});
