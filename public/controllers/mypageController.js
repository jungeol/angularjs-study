angular.module("krTutor")
.constant("tutorListActiveClass","btn-primary")
.constant("productListPageCount", 3)
.controller("mypageController", function($scope, $filter, $window, bookmark, tutorListActiveClass, productListPageCount){

	//bookmark
	$scope.bookmarkData = bookmark.getBookmarks();
	$scope.selectedPage = 1;
	$scope.pageSize = productListPageCount;

	$scope.goBack = function(){
		var address = $scope.getBookmarkGroup();
		$window.location.href = "#/"+address;
	}

	$scope.getBookmarkGroup = function(){
		//console.log($scope.user.group);
		var address = '';
		var group = $scope.data.group;

		switch($scope.user.group){
			case group[1]:
				address = group[2];
				break;
			case group[2]:
				address = group[1];
				break;
			default:
				address = '';
		}

		return address;
	}

	$scope.remove = function(data){
		bookmark.removeBookmark(data);
	}

	//page
	$scope.selectedFilter = function(){
		$scope.selectedPage = 1;
	}
	$scope.selectPage = function(newPage){
		$scope.selectedPage = newPage;
	}
	$scope.getPageClass = function(page){
		return $scope.selectedPage == page ? tutorListActiveClass : "";
	}
});
