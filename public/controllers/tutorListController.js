angular.module("krTutor")
.constant("tutorListActiveClass","btn-primary")
.constant("productListPageCount", 3)
.controller("tutorListCtrl",function($scope, $filter, tutorListActiveClass, productListPageCount, bookmark){
  var data = $scope.data;
  var lang = $scope.lang;

  //filter
  $scope.order = $scope.data.orderBy[0].value;
  //page
  $scope.selectedPage = 1;
  $scope.pageSize = productListPageCount;

  //filter
  $scope.filtersFn = function(tutor){
    var bool = true;
    //kr en 구분해야함
    var type = tutor.type.split(", ");
    var region = tutor.region.split(", ");

    bool = innerFilter(data.type,type,bool);
    bool = innerFilter(data.region,region,bool);

    return bool;
  };
  var innerFilter = function(data,tutorData,b1){
    for(var i=0; i<data.length; i++){
      if(data[i].selected){
        var b2 = false;
        for(var j=0; j<tutorData.length; j++){
          if(data[i][lang] == tutorData[j]){
            b2 = true;
            break;
          }
        }
        b1 = b1 && b2;
      }
    }
    return b1;
  };

  //page
  $scope.selectedFilter = function(){
    $scope.selectedPage = 1;
  }
  $scope.selectPage = function(newPage){
    $scope.selectedPage = newPage;
  }
  $scope.getPageClass = function(page){
    return $scope.selectedPage == page ? tutorListActiveClass : "";
  }
  $scope.addTutorToBookmark = function(tutor){
    bookmark.addBookmark(tutor);
  }
});
