/**
 * Created by Eunseok on 2015-11-06.
 */
// Invoke 'strict' JavaScript mode
'use strict';

// Create the 'articles' service
angular.module('students').factory('Students', ['$resource', function($resource) {
    // Use the '$resource' service to return an article '$resource' object
    return $resource('api/students/:studentId', {
        articleId: '@_id'
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);
