/**
 * Created by Eunseok on 2015-11-06.
 */

angular.module('students')
    .controller('StudentsCtrl', ['$scope', '$routeParams', '$location',
        function($scope, $routeParams, $location) {


            // 스튜던트 생성 함수
            $scope.create = function() {

                // Use the form fields to create a new article $resource object
                var user = new Users({
                    title: this.title,
                    content: this.content
                });

                // Use the article '$save' method to send an appropriate POST request
                article.$save(function(response) {
                    // If an article was created successfully, redirect the user to the article's page
                    $location.path('articles/' + response._id);
                }, function(errorResponse) {
                    // Otherwise, present the user with the error message
                    $scope.error = errorResponse.data.message;
                });
            };

            // 학생 전체 목록 불러오기 함수
            $scope.find = function () {

            };

            // 학생 정보 업데이트 함수
            $scope.update = function () {

            };

        }
    ]);