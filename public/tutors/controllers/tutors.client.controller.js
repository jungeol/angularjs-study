angular.module("krTutor")
    .constant("dataUrl", "http://localhost:5500/tutors")
    .controller("krTutorCtrl", function ($scope, $http, dataUrl) {

        //ui-bootstrap, navbar-collapse boolean
        //$scope.isCollapsed = true;

        $scope.lang = "kr";
        $scope.text = {
            nav1: {en: "Foreigner", kr: "외국인학생"},
            nav2: {en: "KoreanTutor", kr: "한국인튜터"},
            nav3: {en: "MyPage", kr: "마이페이지"},
            nav4: {en: "Login", kr: "로그인"},
        };

        $scope.data = {
            group: [
                "admin",
                "students",
                "tutors"
            ],
            type: [
                {kr: "스피킹", en: "Speaking", selected: false},
                {kr: "문법", en: "Grammer", selected: false},
                {kr: "시험", en: "Exam", selected: false},
            ],
            region: [
                {kr: "동구", en: "donggu", selected: false},
                {kr: "중구", en: "junggu", selected: false},
                {kr: "서구", en: "seogu", selected: false},
                {kr: "유성구", en: "yuseonggu", selected: false},
                {kr: "대덕구", en: "deakeokgu", selected: false},
            ],
            orderBy: [
                {kr: "등급", en: "Tier", value: "tier"},
                {kr: "추천", en: "Recommended", value: "-recommended"},
                {kr: "비용", en: "Price", value: "price"},
            ]
        };


        //임시 로그인 데이터
        $scope.user = {
            //mongo db id
            _id: "whatever",

            //classify : students,tutors,admin
            group: "students",

            //user info : id,pass,name,age,gender, ...??
            userid: "jungol",
            passward: "1234",

            name: "jungeol",
            age: 26,
            gender: "M"
        };
        $scope.user = "";


        $http.get(dataUrl)
            .success(function (data) {
                $scope.data.tutors = data;
            })
            .error(function (error) {
                $scope.data.error = error;
            });


////////////////////////////////////////////////////
////////////////////////////////////////////////
        //유저 DB 정보 데이터 예시
        /*
         db.krTutor.user = {
         id:"MongoDB ID",
         username:"가입 email",
         password:"비번",
         age:"나이",
         gender:"성별",
         img:"이미지 경로(페북 or 서버)",
         group:"학생 or 튜터 or admin",
         info:"group 정보(오브젝트)"
         }
         tutor.info = {
         // 썸네일 부분.
         title:"썸네일 제목", //영어,한국어 구분 필요
         name:{en:"영어이름",kr:'한국이름'}, //혹은 enName,krName으로 구분
         tier:"1~4 등급",
         recommended:"추천 숫자",
         region:"지역 5개", //데이터 형태 어떻게? 배열or문자열or오브젝트
         type:"시험,스피킹,문법", //데이터 형태 어떻게? 배열or문자열or오브젝트
         price:"0~50 예상금액",
         //추가 부분.

         }
         student.info = {

         }
         */

        ////////////////////
        //  임시, 지워도됨
        ///////////////////
        /* 서버에 없을경우 임시데이터
         $scope.data.tutors = [
         {
         title:"BEST Tutor",
         name:{kr:"심준걸",en:"Jungeol Sim"},age:26,gender:"남",
         tier:1,recommended:100,
         region:"유성구",
         type:"스피킹, 문법",
         price:10
         },
         {
         title:"BEST Tutor",
         name:{kr:"심준걸",en:"Jungeol Sim"},age:26,gender:"남",
         tier:2,recommended:20,
         region:"유성구, 중구, 서구",
         type:"시험, 스피킹",
         price:15
         },
         {
         title:"BEST Tutor",
         name:{kr:"심준걸",en:"Jungeol Sim"},age:26,gender:"남",
         tier:3,recommended:90,
         region:"중구, 대덕구",
         type:"스피킹, 문법, 시험",
         price:20
         },
         {
         title:"BEST Tutor",
         name:{kr:"심준걸",en:"Jungeol Sim"},age:26,gender:"남",
         tier:1,recommended:50,
         region:"유성구",
         type:"문법",
         price:30
         },
         {
         title:"BEST Tutor",
         name:{kr:"심준걸",en:"Jungeol Sim"},age:26,gender:"남",
         tier:4,recommended:40,
         region:"유성구",
         type:"시험",
         price:9
         },
         ]*/

    });
