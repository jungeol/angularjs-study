/**
 * Created by Eunseok on 2015-11-06.
 */
'use strict';

angular.module('krTutor').config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/tutors', {
                templateUrl: 'tutors/views/list-tutors.client.view.html'
            }).
            when('/tutors/:tutorId', {
                templateUrl: 'articles/views/view-tutor.client.view.html'
            }).
            when('/tutors/:tutorId', {
                templateUrl: 'articles/views/edit-tutor.client.view.html'
            });

    }
]);

//$routeProvider.when("/mypage", {
//    templateUrl: "/views/mypage.html"
//})
//$routeProvider.when("/tutors", {
//    templateUrl: "/views/tutorList.html"
//})
//$routeProvider.when("/students", {
//    templateUrl: "/views/studentsList.html"
//})
//$routeProvider.when("/login", {
//    templateUrl: "/views/login.html"
//})
//$routeProvider.when("/signup",{
//    templateUrl: "/views/signup.html"
//})
//$routeProvider.otherwise({
//    templateUrl: "/views/main.html"
//});

