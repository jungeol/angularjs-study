/**
 * Created by es-mac on 15. 11. 14..
 */

var users = require('../controllers/users.server.controller.js'),
    tutors = require('./tutor.sever.routes'),
    students = require('./student.server.routes'),
    router = require('express').Router();

// 테스트 모델
var User = require('../models/user.server.model'),
    Student = require('../models/student.server.model'),
    Tutor = require('../models/tutor.server.model');

router.get('*', function (req, res, next) {
    console.log("API 페이지");
    next();
});

// 학생 API
router.use('/students', students);

// 선생 API
router.use('/tutors', tutors);

var tmpPostSignup = function (req, res) {
    console.dir(req.body);
    return res.json(req.body);
};

router.route('/signup')
    .get(users.renderSignup);


router.get('/setup-admin', function () {

});


module.exports = router;
