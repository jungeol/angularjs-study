'use strict';

var router = require('express').Router();
var index = require('../controllers/index.server.controller');
var passport = require('../../config/passport');
var mongoose = require('mongoose');
//var User = mongoose.mode
var User = require('../models/user.server.model');


router.use(function (req, res, next) {
    console.log('%s %s %s', req.method, req.url, req.path);
    next();
});

router.get('/', function (req, res) {
    res.render('index');
});

router.route('/login')
    .get(function (req, res) {
        //res.render('signin');
        console.log("로그인 창");
        res.render('signin', {message: req.flash('loginMessage')});
    })
    .post(passport.authenticate('local-login', {
        successRedirect: '/profile',
        failureRedirect: '/login',
        failureFlash: true
    }));


router.route('/signup')
    .get(function (req, res) {
        res.render('signup', {message: req.flash('signupMessage')});
    });

router.get('/profile', isLoggedIn, function (req, res) {
    res.render('profile', {
        user : req.user
    });
});


function isLoggedIn(req, res, next) {

    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}


router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});


var getErrorMessage = function (err) {
    if (err.errors) {
        for (var errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Unknown Server Error';
    }
};


router.get('/test', function (req, res) {
    User.find().exec(function (err, users) {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.json(users);
        }
    });
});

module.exports = router;
