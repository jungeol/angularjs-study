/**
 * Created by Eunseok on 2015-11-13.
 */
'use strict';

var users = require('../controllers/users.server.controller.js'),
    tutors = require('../controllers/tutors.server.controller'),

    router = require('express').Router();

var passport = require('../../config/passport');


router.route('/')
    .get(tutors.list)
    .post(passport.authenticate('local-signup-tutor', {
        successRedirect: '/profile',
        failureRedirect: '/signup',
        failureFlash: true
    }));

//router.param('tutorId', tutors.tutorId);

router.route('/:tutorID')
    .get(function (req, res, next, tutorID) {
        res.end(tutorID);
    })
    .put()
    .delete();

module.exports = router;




