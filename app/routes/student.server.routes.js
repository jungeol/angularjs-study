/**
 * Created by Eunseok on 2015-11-13.
 */
'use strict';
var users = require('../controllers/users.server.controller.js'),
    students = require('../controllers/students.server.controller'),
    router = require('express').Router();

var passport = require('../../config/passport');


router.use(function (req, res, next) {
    console.log('%s %s %s', req.method, req.url, req.path);
    next();
});

// 스튜던트 생성
router.route('/')
    .get(students.list)
    //.post(function (req, res){
    //
    //});
    .post(passport.authenticate('local-signup-student', {
        successRedirect: '/profile',
        failureRedirect: '/signup',
        failureFlash: true
    }));

// 스튜던트 아이디 패ㄹ미터 값
router.param('studentId', function(req, res, next, studentId) {
    console.log('studentId 출력');
});

router.route('/:studentId', function (req, res, next) {

}).get(function (req, res) {
    res.end(req.params.studentId);
}).put(function(req, res){
    var id = req.params.studentId;
});

router
    .route('/:studentId/recommend')
    .post(function(req, res, next){
         //학생이 추천함 Login Required
         //추천 숫자 증가, 추천인 아이디 저장해서 중복 추천 방지
         //학생이 추천했는지 안q했는지 확인하는 작업 필요

    });



module.exports = router;