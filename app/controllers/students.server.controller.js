/**
 * Created by Eunseok on 2015-11-06.
 */
'use strict';

var mongoose = require('../../config/mongoose')(),
    Student = mongoose.model('Student'),
    User = mongoose.model('User'),
    passport = require('../../config/passport');

var LocalStrategy = require('passport-local').Strategy;


// 에러 처리
var getErrorMessage = function (err) {
    if (err.errors) {
        for (var errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Unknown Server Error';
    }
};

exports.list = function (req, res) {


    Student.find()
        .sort('-created')
        .populate('user_id', '-password -salt')
        .exec(function (err, students) {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.json(students);
        }
    });


};

exports.create = function (req, res) {

};

exports.read = function (req, res) {

};

exports.studentById = function (req, res, next, id) {

};

exports.findStudentByName = function (req, res, next, name) {

};

exports.renderSignup = function (req, res) {

};




