/**
 * Created by Eunseok on 2015-11-06.
 */
'use strict';

var mongoose = require('mongoose'),
    Tutor = mongoose.model('Tutor'),
    User = mongoose.model('User'),
    passport = require('../../config/passport');

//var LocalStrategy = require('passport-local').Strategy;


// 에러 처리
var getErrorMessage = function (err) {

    var message = '';


    if (err.code) {
        switch (err.code) {

            case 11000:
            case 11001:
                message = 'Username already exists';
                break;

            default:
                message = 'Something went wrong';
        }
    } else {

        for (var errName in err.errors) {
            if (err.errors[errName].message) message = err.errors[errName].message;
        }
    }

    return message;
};


// 트랜잭션 처리를 통해서 User Profile 을 먼저 생성하고
// 생성 중간에 오류가 나면 둘다 취소하게 한다.

exports.list = function (req, res) {
    Tutor.find()
        .sort('-created')
        .populate('user_id', '-password -salt')
        .exec(function (err, tutors) {
            if (err) {
                return res.status(400).send({
                    message: getErrorMessage(err)
                });
            } else {
                res.json(tutors);
            }
        });
};

// Tutor 프로파일 생성
exports.create = function (req, res) {

    //테스트용
    //res.json(req.body);


    if (!req.user) {

        if (req.query.callbackUrl)
            res.redirect(req.query.callbackUrl);

        User.findOne({'email': email}, function (err, user) {
            if (err) {

            }
        });

    } else {
        res.redirect('/');
    }
    //
    //var user = new User(req.body);
    //user.save(function (err, thor) {
    //    if (err)
    //        return console.error(err);
    //    //return res.json(err);
    //
    //    console.dir(thor);
    //});

    //
    //var tutor = new Tutor();
    //tutor.user_id = user._id;
    //// 스튜던트 특수항목
    //
    //// 주소 처리
    //var tmp_user = tutor;
    //switch (req.body.addr)
    //{
    //    case 'yuseongu':
    //        tmp_user.addr.yuseongu = true;
    //        break;
    //    case 'seogu':
    //        tmp_user.addr.dongu = true;
    //        break;
    //    case 'daedukgu':
    //        tmp_user.addr.daedukgu = true;
    //        break;
    //    case 'dongu':
    //        tmp_user.addr.dongu = true;
    //        break;
    //    case 'jungu':
    //        tmp_user.addr.jungu= true;
    //        break;
    //}
    //
    ////student.cert_level = req.body.certlevel;
    //
    //tutor.save(function (err, thor) {
    //    if (err) {
    //        user.remove(function(err, user){
    //            if (err) return handleError(err);
    //            User.findById(user._id, function (err, user) {
    //                console.log(user);
    //            })
    //        });
    //        return console.error(err);
    //    }
    //    //console.dir(thor);
    //    console.info(thor);
    //});


};

exports.delete = function (req, res) {

};

exports.put = function (req, res) {

};

exports.renderSignup = function (req, res) {

};

exports.renderEdit = function (req, res) {

};

//exports.tutorID = function(req, res, next, tutorID){
//    req.tutorID = req.param.tutorID;
//};

exports.findTutorByEmail = function (req, res, next) {

};

exports.findTutorByName = function (req, res, next) {

};