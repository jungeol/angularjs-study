// Invoke 'strict' JavaScript mode
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TutorSchema= new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    title: String,
    user_id: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    cert_level: {
        type: Number,
        required: true,
        enum: [1, 2, 3, 4]
    },
    recommended: {
        type: Number,
        default: 0
    },
    addr: {
        seogu: false,
        yuseongu: false,
        jungu: false,
        dongu: false,
        daedukgu: false
    },
    students: [
        {
            type: Schema.ObjectId,
            ref: 'Student'
        }
    ]
});

module.exports = mongoose.model('Tutor', TutorSchema);