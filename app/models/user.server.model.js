// Invoke 'strict' JavaScript mode
'use strict';
var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;

// 유저 Schema 정의
var UserSchema = new Schema({

    email: {
        type: String,
        unique: true,
        required: 'Email is required',
        // 이메일 정규식
        match: [/.+\@.+\..+/, "Please fill a valid email address"]
    },
    name: {
        type: String,
        required: 'Username is required',
        trim: true,
        first: String,
        last: String
    },
    password: {
        type: String,
        required: true,
        validate: [

            function (password) {
                return password && password.length > 6;
            }, 'Password should be longer'
        ]
    },
    age: {
        type: Number,
        required: true
    },
    gender: {
        type: String,
        max: 1,
        required: true
    },
    role: {
        type: String,
        required: true,
        enum: ['student', 'tutor', 'admin']
    },
    salt: {
        type: String
    },
    profile_pic: {
        type: String
    }
    ,
    provider: {
        // 소셜 OAUTH 프로바이더
        type: String
    },
    providerId: String,
    providerData: {},
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
    ,
    last_login: Date,
    contact: [
        String
    ]
}
);

// 저장하기전에 비밀번호 암호화: 솔트 랜덤 생성
UserSchema.pre('save', function (next) {
    if (this.password) {
        this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
        this.password = this.hashPassword(this.password);
    }

    next();
});


UserSchema.methods.hashPassword = function (password) {
    return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
};

// 유저 인증
UserSchema.methods.authenticate = function (password) {
    return this.password === this.hashPassword(password);
};

// Find possible not used username

// 가능한 유니크한 이름
UserSchema.statics.findUniqueUsername = function (username, suffix, callback) {
    var _this = this;

    // Add a 'username' suffix
    var possibleUsername = username + (suffix || '');

    // Use the 'User' model 'findOne' method to find an available unique username
    _this.findOne({
        username: possibleUsername
    }, function (err, user) {
        // If an error occurs call the callback with a null value, otherwise find find an available unique username
        if (!err) {
            // If an available unique username was found call the callback method, otherwise call the 'findUniqueUsername' method again with a new suffix
            if (!user) {
                callback(possibleUsername);
            } else {
                return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
            }
        } else {
            callback(null);
        }
    });
};

// Configure the 'UserSchema' to use getters and virtuals when transforming to JSON
UserSchema.set('toJSON', {
    getters: true,
    virtuals: true
});

// Create the 'User' model out of the 'UserSchema'
module.exports = mongoose.model('User', UserSchema);