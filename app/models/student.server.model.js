// Invoke 'strict' JavaScript mode
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var Tutor = mongoose.model('Tutor');


var studentSchema = new Schema({
    user_id: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    title: String,
    addr: {
        seogu: false,
        yuseongu: false,
        jungu: false,
        dongu: false,
        daedukgu: false
    },
    avail_tutoring_num: {},
    wish_teachers: [{
        type: Schema.ObjectId,
        ref: 'Tutor'
    }],
    subjects: [
        String
    ]
    ,
    favor_places: [
        String
    ],
    Introduction: String,
    is_group: Boolean,
    fee: {
        min: {
            type: Number,
            required: true
        },
        max: {
            type: Number,
            required: true
        }        
    },
    created: {
        type: Date,
        default: Date.now
    }
});


module.exports = mongoose.model('Student', studentSchema);